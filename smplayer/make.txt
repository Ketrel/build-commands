LDFLAGS='-Wl,-rpath,/built/lib' make \
    PREFIX=/built \
    QMAKE_OPTS=DEFINES+=NO_DEBUG_ON_CONSOLE \
    LRELEASE=lrelease \
    -j4 \
    install
